# krcmd Binary

A command line utility to communicate with Kawasaki Robots using Krcc DLL. 

## Getting started

Copy this executable along with Krcc64.dll to the same folder. You can find Krcc from your local Kawasaki Distributor.

Type krcmd -h for help.


This software provided AS IS and might contain bugs. Use it at your own risk. No liability is accepted.

